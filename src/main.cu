#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iostream>
#include <map>
#include <numeric>
#include <random>
#include <sys/time.h>
#include <valarray>

#include "range.hpp"
#include "utils.hpp"

#define NUM_ROWS 28
#define NUM_COLS 28
#define NUM_CHANNELS 1
#define NUM_DIGITS 10
#define TILE_WIDTH_GIVEN 4
#define KERNEL_WIDTH 5

static size_t FLAGS_batch_size = 1000;
static std::string FLAGS_testdata{};
static std::string FLAGS_model{};

// Data and reference data dimensions
shape xdims          = {FLAGS_batch_size, NUM_CHANNELS, NUM_ROWS, NUM_COLS};
static int xdims_g[] = {1000, NUM_ROWS, NUM_COLS, NUM_CHANNELS};
shape rdims = {FLAGS_batch_size, NUM_DIGITS};

static int conv1dims_g[] = {5, 5, 1, 32}; //32 filters
static int conv2dims_g[] = {5, 5, 32, 64};
shape conv1dims = {32, 1, 5, 5};
shape conv2dims = {64, 32, 5, 5};

static int fc1dims_g[]   = {1024, 128};
static int fc2dims_g[]   = {128, 10};
shape fc1dims   = {1024, 128};
shape fc2dims   = {128, 10};

__constant__ float dev_cons_Wunroll[15200];
//__constant__ float dev_cons_W[sizeof(float)*25*32];

/******************************************************************************
 GPU Kernels
*******************************************************************************/
__global__ void unroll(const float *x, float *x_unroll, float *t_x_unroll){
    // 5 * 5 from filter size (per channel)
    const unsigned int x_unroll_height = 25;
    // 24 * 24 from the convolution output size
    const unsigned int x_unroll_width = 576;
    // every block unrolls 1 batch, every batch contains 1 input in the 1st convolution
    // channel_idx is the block index
    const unsigned int channel_idx = blockIdx.x;
    // the amount of addresses in one channel
    const unsigned int address_per_channel = x_unroll_height * x_unroll_width;
    // load all inputs of 1 batch into shared memory, which is 28 * 28 * 1 for the 1st convolution
    __shared__ float x_shared[784];
    const unsigned int x_base = channel_idx * 784;
    for (unsigned int i = threadIdx.x; i < 784; i += blockDim.x) {
        x_shared[i] = x[x_base + i];
    }
    __syncthreads();
    unsigned int t = threadIdx.x;
    while (t < x_unroll_width) {
        // starting row number in the current channel
        const unsigned int x_row_base = t / 24;
        // starting col number in the current channel
        const unsigned int x_col_base = t % 24;
        // offset to the first address of the col after unrolling
        unsigned int x_unroll_offset = channel_idx * address_per_channel + t;
        //transpose x offset
        unsigned int t_x_unroll_offset = channel_idx * address_per_channel + t * 25;
        // unroll the matrix
        for (unsigned int i = 0; i < 5; i++) {
            const unsigned int x_start = (x_row_base + i) * 28;
            for (unsigned int j = 0; j < 5; j++) {
                x_unroll[x_unroll_offset] = x_shared[x_start + (x_col_base + j)];
                x_unroll_offset += x_unroll_width;
                t_x_unroll[t_x_unroll_offset] = x_shared[x_start + (x_col_base + j)];
                t_x_unroll_offset ++;
                //test
                // x_unroll[x_unroll_offset] = 12;
                //x_unroll_offset += x_unroll_width;
            }
        }
        // every block has only 512 threads for better GPU utilization
        // but width of unrolled matrix is 576, so the first 64 needs to do another round
        t += 512;
    }
    //test
    
}


__global__ void gpu_backward_ygrad(const float *Y_orig, float *Y){
    int start = blockIdx.x * 18432;
    for(int i =0 ; i<32;i++){
        int index = start + threadIdx.x  +i*576;
        Y[index] = Y[index] - Y_orig[index];
        Y[index] = (Y[index] <= 0) ? 0 : Y[index];
    }
}
__global__ void gpu_backward_dw(const float* A, const float* B, float* C,
                                const int numARows, const int numAColumns,
                                const int numBRows, const int numBColumns,
                                const int numCRows, const int numCColumns, const int num_of_X_tiles, int batch){
    const int TILE_WIDTH = 32; //Set Tile_Width as 32
    //Assertions to check my matrix sizes
    if(batch == 0){
        if(threadIdx.x < 25 && threadIdx.y <32){
            C[threadIdx.x + threadIdx.y * 25] = 0;
        }
    }
    assert(numAColumns == numBRows);
    assert(numARows == numCRows);
    assert(numBColumns == numCColumns);
    __shared__ float subTileA[TILE_WIDTH][TILE_WIDTH];
    __shared__ float subTileB[TILE_WIDTH][TILE_WIDTH];
    int tx = threadIdx.x; int ty = threadIdx.y;
    int bx = blockIdx.x;  int by = blockIdx.y;
    int Col = tx + TILE_WIDTH * blockIdx.x;
    int Row = ty + TILE_WIDTH * blockIdx.y;
    float Cvalue = 0;
    //Load only X into shared memory
    for(int m=0; m < num_of_X_tiles; ++m ){
        if((Row < numCRows) && (m*TILE_WIDTH+tx) < numAColumns)
            subTileA[ty][tx] = A[batch * 32 * 24*24 + Row*numAColumns+m*TILE_WIDTH+tx];
        else
            subTileA[ty][tx] = 0;
        if((Col < numCColumns) && (m*TILE_WIDTH+ty) < numBRows)
            subTileB[ty][tx] = B[batch * 24*24*25  + (m*TILE_WIDTH+ty)*numBColumns+Col];
        else
            subTileB[ty][tx] = 0;
        
        __syncthreads();
        for(int k=0; k < TILE_WIDTH; ++k){
            Cvalue += subTileA[ty][k] * subTileB[k][tx];
        }
        __syncthreads();
    }
    if((Row < numCRows) && (Col < numCColumns)){
        int h = Col/5;
        int w = Col%5;
        int i = batch;
        int m = Row;
        // int yoffset = ((i * outputdims[1] + h) * outputdims[2] + w) * outputdims[3] + m;
        int yoffset = m * 25 +h*5+w;
        //C[yoffset] = Cvalue;
        C[yoffset] += Cvalue;
        C[yoffset] = (C[yoffset] < 0) ? 0: C[yoffset];  // Doing relu4 right here
    }
}





//1st kernel will put convolution filters into matrix
//2nd kernel puts input feature maps into a matrix
//3rd kernel will do the multiplication

/*This kernel will remap X from X[batch_size, height, width, # of input feature map] to X[batchsize,# of feature map, height, width]
 dim3 ReMapBlock(32,32,1);
 //Each Thread block processes all channels in 28 and 28 image
 Remap_x = ceil( xdims[2]/32.0f); // BlockIDx.x*blockDim.x+threadIdx.x is column number //1
 Remap_y = ceil( xdims[1]/32.0f); // BlockIDx.y*blockDim.y+threadIdx.y is row number    //1
 Remap_z = ceil( xdims[0]/1.0f);  // BlockIDx.z*blockDim.z+threadIdx.z is batch number  //10
 dim3 ReMapGrid(Remap_x, Remap_y, Remap_z);*/
//Took 16us for 1st convo, took 82us for 2nd convo
__global__ void n_RemapX(const float*X, const int* xdims, float* Xremapped){
    //if(xdims[3] == 1)
    //	return;
    //inputindex =  i * xdims[1] * xdims[2] * xdims[3]
    //				+ height * xdims[2] * xdims[3]
    //				+  width * xdims[3] + channel;
    int batch = blockIdx.z*blockDim.z + threadIdx.z;
    int row = blockIdx.y*blockDim.y + threadIdx.y;
    int col = blockIdx.x*blockDim.x + threadIdx.x;
    int channel = 0; //Channel is 0 at start
    
    const int inputindex = 	batch * xdims[1] * xdims[2] * xdims[3] +  		// cancelled out ( blockIdx.z/xdims[1] ) * xdims[1]
    row * xdims[2] * xdims[3] +
    col * xdims[3] + channel;
    //outputindex = i * xdims[1] * xdims[2] * xdims[3] +
    //				channel * xdims[1] * xdims[2] +
    //				height * xdims[2] + width;
    const int outputindex = batch * xdims[1] * xdims[2] * xdims[3] + 		//( blockIdx.z/xdims[1] ) * xdims[1]
    channel * xdims[1] * xdims[2] +
    row * xdims[2] + col;
    if(row < xdims[1] && col < xdims[2] && batch < xdims[0] ){
        for(channel = 0; channel < xdims[3]; channel++ )
            Xremapped[outputindex + channel*xdims[1]*xdims[2] ] = X[inputindex + channel];
    }
}



/*
	dim3 z_unrolGrid(xdims[3], xdims[0], 1);  // one block for only an input map, xdims[3]= blockIdx.x means input map no. for one batch, blockIdx.y is batch size
 dim3 z_unrolBlock(xdims[1], xdims[2], 1);   // 2d block, x & y is input width and height, */
__global__ void z_unrolX_remapped(const float* X, const int* Xdims, float* X_unrol){
    __shared__ float temp[28*28]; //one block only take care of one input feature map, shared memory only store for one inputfeture map elements, highest is first convolution
    
    int channel_i = blockIdx.x; //input no., in the case of 2ed convolution, it's from 0 to 31, 32 input in total
    //remapped index
    //outputindex = i * xdims[1] * xdims[2] * xdims[3] +
    //				channel * xdims[1] * xdims[2] +
    //				height * xdims[2] + width;
    int Xoffset = blockIdx.y * Xdims[1] * Xdims[2] * Xdims[3] + channel_i* Xdims[2] * Xdims[1] + threadIdx.y * Xdims[2] + threadIdx.x ;
    //blockIdx.y is batch no.,
    int tempoffset = threadIdx.y * Xdims[2] + threadIdx.x; // 2 dimention block, shared memory is 2D, one thread copy one element into shared memory, in 2ed convolution, totaly 12*12 elements= threads number in block
    temp[tempoffset] = X[Xoffset];
    //temp[tempoffset] = tempoffset;
    __syncthreads();
    
    //Row in grid means batch, row 0 means batch 0. Column means input no.,
    //Blocks in Row 0 Column 0 will write the first 25 (0~24)rows of unrow matrix of batch0
    //Blocks in Row 0 Column 1 will write the second 25 (25~49)rows of unrow matrix of batch0
    //Block dims is 12 * 12 in second convolution, but only 8*8 threads work, each thread write a whole column of 25 elements.
    //unrow matrix is global memory, write in the global memory, different thread write into different element, no need to atomic add
    //threads only need to read data from shared memory and write into global memory, fast enough
    //Problem:
    //1.may have control divergence, not all threads in one block are working
    //2.second convolution, the block is too small, only 144 threads, and have 32 * batch no. blocks in total, maybe too mank blocks but each block has few threads
    
    int outputwidth = Xdims[1] - 5 + 1; //output feature map width, equal to height
    int unrollwidth = outputwidth * outputwidth;
    if((threadIdx.x < outputwidth) && (threadIdx.y < outputwidth)){  // we only need output feature map elements number of threads to work, 8*8 in the second convolution
        //recall that blockIdx.x is input map no., blockIdx.y is batch no.
        int unroll_col = threadIdx.y * outputwidth + threadIdx.x;   // col index in unroll matrix
        int unroll_row_base = 5 * 5 * Xdims[3] * blockIdx.y + blockIdx.x * 5 * 5;
        int i = 0;
        
        for(int p = 0; p < 5; p++)
            for(int q = 0; q < 5; q++){
                X_unrol[(unroll_row_base + i) * unrollwidth + unroll_col] = temp[(threadIdx.y + p) * Xdims[2] + (threadIdx.x + q) ];   // not sure about the p and q, I think it doesn't matter
                //X_unrol[(unroll_row_base + i) * unrollwidth + unroll_col] = 8;
                //X_unrol[(unroll_row_base + i) * unrollwidth + unroll_col] += 1;
                i++;
                
            }
    }
    
}


__global__ void z_unrollw(const float* originallayout, float* multiplylayout, const int* filterdims){
    //filterdims is wdims, col and row is in unrollw
    //recall the threadIdx.x and threadIdx.y is 5, threadIdx.z is input no., blockIdx.x is output no.
    int col = threadIdx.z * 5 * 5 + threadIdx.y * 5 + threadIdx.x; // not sure about the x and y, 90% sure
    int row = blockIdx.x;    //for filters calculating output map0, stored in row0.
    //this multiplylayout is 2D array, have total 25*wdims[2]*wdims[3] elements, same as threads in grid.
    multiplylayout[row * filterdims[2] * 5 * 5 + col] = originallayout[threadIdx.y * filterdims[1] * filterdims[2] * filterdims[3] + threadIdx.x * filterdims[2] * filterdims[3] + threadIdx.z * filterdims[3] + blockIdx.x];
}

__global__ void z_MatrixMultiply_DEDW(const float* A, const float* B, float* C,
                                          const int numARows, const int numAColumns,
                                          const int numBRows, const int numBColumns,
                                          const int numCRows, const int numCColumns,
                                          const int num_of_X_tiles, int batch ){
                                          //const int* outputdims, const int* inputdims, ){
                                      
    const int TILE_WIDTH = 32; //Set Tile_Width as 32
    //Assertions to check my matrix sizes
    if(batch == 0){
        if(threadIdx.x < 25 && threadIdx.y <32){
            C[threadIdx.x + threadIdx.y * 25] = 0;
        }
    }
    assert(numAColumns == numBRows);
    assert(numARows == numCRows);
    assert(numBColumns == numCColumns);
    __shared__ float subTileA[TILE_WIDTH][TILE_WIDTH];
    __shared__ float subTileB[TILE_WIDTH][TILE_WIDTH];
    int tx = threadIdx.x; int ty = threadIdx.y;
    int bx = blockIdx.x;  int by = blockIdx.y;
    int Col = tx + TILE_WIDTH * blockIdx.x;
    int Row = ty + TILE_WIDTH * blockIdx.y;
    float Cvalue = 0;
    //Load only X into shared memory
    for(int m=0; m < num_of_X_tiles; ++m ){
        if((Row < numCRows) && (m*TILE_WIDTH+tx) < numAColumns)
            subTileA[ty][tx] = A[batch * 32 * 24*24 + Row*numAColumns+m*TILE_WIDTH+tx];
        else
            subTileA[ty][tx] = 0;
        if((Col < numCColumns) && (m*TILE_WIDTH+ty) < numBRows)
            subTileB[ty][tx] = B[batch *  25*24*24 + (m*TILE_WIDTH+ty)*numBColumns+Col];
            //subTileB[ty][tx] = B[batch * outputdims[0] * outputdims[1] * 24*24  + (m*TILE_WIDTH+ty)*numBColumns+Col];
        else
            subTileB[ty][tx] = 0;
        
        __syncthreads();
        for(int k=0; k < TILE_WIDTH; ++k){
            Cvalue += subTileA[ty][k] * subTileB[k][tx];
        }
        __syncthreads();
    }
    if((Row < numCRows) && (Col < numCColumns)){
        int h = Col/5; //outputdims[1];
        int w = Col%5; //outputdims[1];
        int i = batch;
        int m = Row;
        //int yoffset = ((i * outputdims[0] + h) * outputdims[1] + w) * outputdims[3] + m;
        int yoffset = m * 25 +h*5+w;
        //int yoffset = i*outputdims[1]*outputdims[2]*outputdims[3] + m * outputdims[1]* outputdims[2]+h*outputdims[2]+w;
        C[yoffset] += Cvalue;
        C[yoffset] = (C[yoffset] < 0) ? 0: C[yoffset]; // Doing relu4 right here
    }
}


__global__ void z_MatrixMultiply_perbatch(const float* A, const float* B, float* C,
                                          const int numARows, const int numAColumns,
                                          const int numBRows, const int numBColumns,
                                          const int numCRows, const int numCColumns,
                                          const int num_of_X_tiles, const int* outputdims,
                                          const int* inputdims, int batch){
    const int TILE_WIDTH = 32; //Set Tile_Width as 32
    //Assertions to check my matrix sizes
    assert(numAColumns == numBRows);
    assert(numARows == numCRows);
    assert(numBColumns == numCColumns);
    __shared__ float subTileA[TILE_WIDTH][TILE_WIDTH];
    __shared__ float subTileB[TILE_WIDTH][TILE_WIDTH];
    int tx = threadIdx.x; int ty = threadIdx.y;
    int bx = blockIdx.x;  int by = blockIdx.y;
    int Col = tx + TILE_WIDTH * blockIdx.x;
    int Row = ty + TILE_WIDTH * blockIdx.y;
    float Cvalue = 0;
    //Load only X into shared memory
    for(int m=0; m < num_of_X_tiles; ++m ){
        if((Row < numCRows) && (m*TILE_WIDTH+tx) < numAColumns)
            subTileA[ty][tx] = A[Row*numAColumns+m*TILE_WIDTH+tx];
        else
            subTileA[ty][tx] = 0;
        if((Col < numCColumns) && (m*TILE_WIDTH+ty) < numBRows)
            subTileB[ty][tx] = B[batch * outputdims[1] * outputdims[2] * 25 * inputdims[3]  + (m*TILE_WIDTH+ty)*numBColumns+Col];
        else
            subTileB[ty][tx] = 0;
        
        __syncthreads();
        for(int k=0; k < TILE_WIDTH; ++k){
            Cvalue += subTileA[ty][k] * subTileB[k][tx];
        }
        __syncthreads();
    }
    if((Row < numCRows) && (Col < numCColumns)){
        int h = Col/outputdims[2];
        int w = Col%outputdims[2];
        int i = batch;
        int m = Row;
        // int yoffset = ((i * outputdims[1] + h) * outputdims[2] + w) * outputdims[3] + m;
        int yoffset = i*outputdims[1]*outputdims[2]*outputdims[3] + m * outputdims[1]* outputdims[2]+h*outputdims[2]+w;
        C[yoffset] = Cvalue;
        //C[yoffset] = (Cvalue < 0) ? 0: Cvalue;	// Doing relu4 right here
    }
}


__global__ void z_MatrixMultiply_perbatch_constant(const float* B, float* C,
                                                   const int numARows, const int numAColumns,
                                                   const int numBRows, const int numBColumns,
                                                   const int numCRows, const int numCColumns, const int num_of_X_tiles, const int* outputdims, const int* inputdims, int batch){
    const int TILE_WIDTH = 32; //Set Tile_Width as 32
    //Assertions to check my matrix sizes
    assert(numAColumns == numBRows);
    assert(numARows == numCRows);
    assert(numBColumns == numCColumns);
    //__shared__ float subTileA[TILE_WIDTH][TILE_WIDTH];
    __shared__ float subTileB[TILE_WIDTH][TILE_WIDTH];
    int tx = threadIdx.x; int ty = threadIdx.y;
    int bx = blockIdx.x;  int by = blockIdx.y;
    int Col = tx + TILE_WIDTH * blockIdx.x;
    int Row = ty + TILE_WIDTH * blockIdx.y;
    float Cvalue = 0;
    //Load only X into shared memory
    for(int m=0; m < num_of_X_tiles; ++m ){
        /* if((Row < numCRows) && (m*TILE_WIDTH+tx) < numAColumns)
         subTileA[ty][tx] = dev_cons_Wunroll[Row*numAColumns+m*TILE_WIDTH+tx];
         else
         subTileA[ty][tx] = 0;*/
        if((Col < numCColumns) && (m*TILE_WIDTH+ty) < numBRows)
            subTileB[ty][tx] = B[batch * outputdims[1] * outputdims[2] * 25 * inputdims[3]  + (m*TILE_WIDTH+ty)*numBColumns+Col];
        else
            subTileB[ty][tx] = 0;
        
        __syncthreads();
        for(int k=0; k < TILE_WIDTH; ++k){
            if((m*TILE_WIDTH+k)<numAColumns){
                //Cvalue += subTileA[ty][k] * subTileB[k][tx];
                Cvalue += dev_cons_Wunroll[Row*numAColumns+m*TILE_WIDTH+k] * subTileB[k][tx];
            }
            else{
                Cvalue += 0;
            }
        }
        __syncthreads();
    }
    if((Row < numCRows) && (Col < numCColumns)){
        int h = Col/outputdims[2];
        int w = Col%outputdims[2];
        int i = batch;
        int m = Row;
        //int yoffset = ((i * outputdims[1] + h) * outputdims[2] + w) * outputdims[3] + m;
        //remapped yoffset
        int yoffset = i*outputdims[1]*outputdims[2]*outputdims[3] + m * outputdims[1]* outputdims[2]+h*outputdims[2]+w;
        C[yoffset] = Cvalue;
        //C[yoffset] = (Cvalue < 0) ? 0: Cvalue;
    }
}


/*This kernel will read convolution result in memory coalesced way and
 do averagepool.
 //One thread represents 1 element in output matrix and merges 4 pixels into 1
 dim3 avgPoolBlock(32,32,1);
 int avgpool_x_coal = ceil( bdims[2]/32.0f); // BlockIDx.x*blockDim.x+threadIdx.x is column number 	//1
 int avgpool_y_coal = ceil( bdims[1]/32.0f); // BlockIDx.y*blockDim.y+threadIdx.y is row number		//1
 int avgpool_z_coal = ceil( bdims[3]/1.0f);  // BlockIDx.z*blockDim.z+threadIdx.z is channel   //32 then 64*/
__global__ void n_averagepool_output_remapped(const float* X, const int* xdims, const int pool_size,
                                              float* Y, const int* ydims, const int batch){
    //inputindex =  i * xdims[1] * xdims[2] * xdims[3]
    //       + row * xdims[2] * xdims[3]
    //       +  col * xdims[3] + channel;
    int col = blockIdx.x*blockDim.x + threadIdx.x;
    int row = blockIdx.y*blockDim.y + threadIdx.y;
    int channel = blockIdx.z*blockDim.z + threadIdx.z;
    //int batch = 0; //Batch is 0 at start
    int outputindex = batch * ydims[1]*ydims[2]*ydims[3] +
    channel * ydims[1]*ydims[2] +
    row * ydims[1] + col;
    int inputindex  = batch * xdims[1]*xdims[2]*xdims[3] +
    channel * xdims[1]*xdims[2] +
    2*row * xdims[1] + 2*col;
    /* int outputindex =  batch * ydims[1] * ydims[2] * ydims[3] +
     row * ydims[2] * ydims[3] +
     col * ydims[3] + channel;*/
    /* int inputindex = batch * xdims[1]* xdims[2]* xdims[3]+
     2*row*xdims[2]*xdims[3] +
     2*col*xdims[3] + channel;*/
    
    if (col < ydims[2] && row < ydims[1] && channel < ydims[3]){
        
        float Cvalue = 0;
        Cvalue += X[inputindex]/4.0;
        Cvalue += X[inputindex+xdims[1]]/4.0;
        Cvalue += X[inputindex+1]/4.0;
        Cvalue += X[inputindex+1+xdims[1]]/4.0;
        
        /*for(int p=0; p < pool_size; p++){ //p is width//poolsize = 2 always
         for(int q=0; q < pool_size; q++){ //q is height
         int inputindex = batch * xdims[1]*xdims[2]*xdims[3]+
         (row+q)*xdims[2]*xdims[3] +
         (col+p)*xdims[3] + channel;
         Cvalue += X[inputindex]/4.0f;
         //Cvalue = Cvalue + A[Row*numAColumns+k] * B[Col+k*numBColumns];
         //Cvalue=Cvalue+A[k][Row] * B[Col][k];
         }
         }*/
        Y[outputindex] = Cvalue; //inputindex; //C[output]= Cvalue;
    }
}

__global__ void n_averagepool_coalesced(const float* X, const int* xdims, const int pool_size,
                                        float* Y, const int* ydims, const int batch){
    //inputindex =  i * xdims[1] * xdims[2] * xdims[3]
    //       + row * xdims[2] * xdims[3]
    //       +  col * xdims[3] + channel;
    int col = blockIdx.x*blockDim.x + threadIdx.x;
    int row = blockIdx.y*blockDim.y + threadIdx.y;
    int channel = blockIdx.z*blockDim.z + threadIdx.z;
    //int batch = 0; //Batch is 0 at start
    /* int outputindex = batch * ydims[1]*ydims[2]*ydims[3] +
     channel * ydims[1]*ydims[2] +
     row * ydims[1] + col;*/
    int inputindex  = batch * xdims[1]*xdims[2]*xdims[3] +
    channel * xdims[1]*xdims[2] +
    2*row * xdims[1] + 2*col;
    int outputindex =  batch * ydims[1] * ydims[2] * ydims[3] +
    row * ydims[2] * ydims[3] +
    col * ydims[3] + channel;
    /* int inputindex = batch * xdims[1]* xdims[2]* xdims[3]+
     2*row*xdims[2]*xdims[3] +
     2*col*xdims[3] + channel;*/
    
    if (col < ydims[2] && row < ydims[1] && channel < ydims[3]){
        
        float Cvalue = 0;
        Cvalue += X[inputindex]/4.0;
        Cvalue += X[inputindex+xdims[1]]/4.0;
        Cvalue += X[inputindex+1]/4.0;
        Cvalue += X[inputindex+1+xdims[1]]/4.0;
        
        /*for(int p=0; p < pool_size; p++){ //p is width//poolsize = 2 always
         for(int q=0; q < pool_size; q++){ //q is height
         int inputindex = batch * xdims[1]*xdims[2]*xdims[3]+
         (row+q)*xdims[2]*xdims[3] +
         (col+p)*xdims[3] + channel;
         Cvalue += X[inputindex]/4.0f;
         //Cvalue = Cvalue + A[Row*numAColumns+k] * B[Col+k*numBColumns];
         //Cvalue=Cvalue+A[k][Row] * B[Col][k];
         }
         }*/
        Y[outputindex] = Cvalue; //inputindex; //C[output]= Cvalue;
    }
}

__global__ void fullyforward_kernel(const float *X, const int* Xdims, const float *W, const int* Wdims,
                             float *Y, const int* Ydims ){
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    int i = index / Ydims[1];  //batch number
    int j = index % Ydims[1];  //element number inside a batch
    float sum = 0;
    
    if(( index< Ydims[0]*Ydims[1])&&(i<Ydims[0])&&(j<Ydims[1])){
            for (int k =0; k < Xdims[1]; k++ ){
                sum += X[i * Xdims[1] + k] * W[k * Wdims[1] + j];
            }  
            Y[i * Wdims[1] + j] = sum;
        }
}


__global__ void conv_backward_ygrad_gpu(const float* in1, const float* in2, const int size, float* out){
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(idx < size)
        out[idx] = in2[idx] - in1[idx];
}

__global__ void relu4_gpu(float* in, int size){
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(idx<size)
        in[idx] = (in[idx] < 0 ) ? 0 : in[idx];
}


/******************************************************************************
 Host Functions
*******************************************************************************/
// From book chapter Figure 16.4
//X is input feature map
//W is convolution filter weights
//Y is output feature map
//W is stored as [p,q,c,m] = [conv height, conv width, channel, output feature map]
//Y is stored as [i,h,w,m]
//X is stored as [batches, height, width, channels ]
//Xdims[0] is # of input feature maps; Xdims[1] is height; Xdims[2] is width; Xdims[3] is channels
//Wdims[0] is height=5; Wdims[1] is width=5; Wdims[2] is channels; Wdims[3] is # of filters per output feature map
//Ydims[0] is # of X input feature maps; Ydims[1] is height after convo; Ydims[2] is width after convo; Ydims[3] is # of output feature maps
static void conv_forward_valid_gpu (const float *X, const int xdims[4],
    const float *W, /*const bool conv1d*/ int wdims[4], const float *W2,
    int wdims2[4], float *Y, const int ydims[4], const int ydims2[4]) {
    
    const auto filter_h   = wdims[0]; //KERNEL_WIDTH; //Height of convolution kernel = 5
    const auto filter_w   = wdims[1]; //KERNEL_WIDTH; //Width = 5
    //const auto in_channel = wdims[2]; //Channel
    
    //Putting wdims into constant memory. It can have only 2 possible values
    float *devX;
    float *devXremapped;
    float *devX_unrol;
    float *devW;
    float *devW_unroll;
    float *devY;
    int *devXdims;
    int *devWdims;
    int *devYdims;
    const int Wsize = sizeof(float) * KERNEL_WIDTH * KERNEL_WIDTH * wdims[2] * wdims[3];
    const int Xsize = sizeof(float) * xdims[0] * xdims[1] * xdims[2] * xdims[3];
    const int Xunrol_size = sizeof(float) * KERNEL_WIDTH * KERNEL_WIDTH *xdims[3] * xdims[0] * (xdims[1]-5+1)*(xdims[1]-5+1) ;
    const int Ysize = sizeof(float) * ydims[1] * ydims[2] * ydims[3] * ydims[0];
    printf("xdims[0]=%d, xdims[1]=%d, xdims[2]=%d, xdims[3]=%d\n",xdims[0],xdims[1],xdims[2],xdims[3]);
    printf("ydims[0]=%d, ydims[1]=%d, ydims[2]=%d, ydims[3]=%d\n",ydims[0],ydims[1],ydims[2],ydims[3]);
    printf("wdims[0]=%d, wdims[1]=%d, wdims[2]=%d, wdims[3]=%d\n",wdims[0],wdims[1],wdims[2],wdims[3]);
    printf("Wsize = %d\n", Wsize);
    printf("Xsize = %d\n", Xsize);
    printf("Xsize_unrol = %d\n", Xunrol_size);
    printf("Ysize = %d\n", Ysize);
    check_success(cudaMalloc(&devX, Xsize));
    check_success(cudaMalloc(&devXremapped, Xsize));
    check_success(cudaMalloc(&devX_unrol, Xunrol_size));
    check_success(cudaMalloc(&devW, Wsize));
    check_success(cudaMalloc(&devW_unroll, Wsize));     // new added to change the way filters store in memory
    check_success(cudaMalloc(&devWdims, sizeof(int)*4));
    check_success(cudaMalloc(&devY, Ysize));
    check_success(cudaMalloc(&devXdims, sizeof(int)*4));
    check_success(cudaMalloc(&devYdims, sizeof(int)*4));
    
    check_success(cudaMemcpy(devX, X, Xsize, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devW, W, Wsize, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devXdims, &xdims[0], sizeof(int)*4, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devYdims, &ydims[0], sizeof(int)*4, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devWdims, &wdims[0], sizeof(int)*4, cudaMemcpyHostToDevice));
    
    
    
    
    
    /*
     for(int w = 0; w < xdims[0]; w++)
     for(int x = 0; x < xdims[1]; x++)
     for(int y =0; y < xdims[2]; y++)
     for(int z =0; z <xdims[3]; z++){
					int index = w*xdims[1]*xdims[2]*xdims[3] + x*xdims[2]*xdims[3] + y*xdims[3] + z;
     X[index] = index;
					}
     check_success(cudaMemcpy(devX, X, Xsize, cudaMemcpyHostToDevice));
     dim3 z_unrolGrid(xdims[3], xdims[0], 1);  	// one block for only an input map, xdims[3]= blockIdx.x means input map no. for one batch, blockIdx.y is batch size
     dim3 z_unrolBlock(xdims[1], xdims[2], 1);   // 2d block, x & y is input width and height,
     z_unrolX<<<z_unrolGrid,z_unrolBlock>>>(devX, devXdims, devX_unrol);
     cudaDeviceSynchronize();
     float* zoe_X_unrolled = (float*) malloc(Xunrol_size);
     check_success(cudaMemcpy(zoe_X_unrolled, devX_unrol, Xunrol_size, cudaMemcpyDeviceToHost));
     
     */
    
    
    
    //Changing this to non-memory coalesced function because its actually memory coalesced when channel = 1
    //This kernel will remap X from X[batch_size, height, width, # of input feature map] to X[batchsize,# of feature map, height, width]
    dim3 ReMapBlock(32,32,1);
    //Each Thread block processes all channels in 28 and 28 image
    int Remap_x = ceil( xdims[2]/32.0f); // BlockIDx.x*blockDim.x+threadIdx.x is column number //1
    int Remap_y = ceil( xdims[1]/32.0f); // BlockIDx.y*blockDim.y+threadIdx.y is row number    //1
    int Remap_z = ceil( xdims[0]/1.0f);  // BlockIDx.z*blockDim.z+threadIdx.z is batch number  //10
    dim3 ReMapGrid(Remap_x, Remap_y, Remap_z);
    n_RemapX<<<ReMapGrid, ReMapBlock>>>(devX, devXdims, devXremapped);
    //	float* Xremapped = (float*) malloc(Xsize);
    /*	check_success(cudaMemcpy(Xremapped, devXremapped, Xsize, cudaMemcpyDeviceToHost));
     for(int w = 0; w < xdims[0]; w++)
     for(int x = 0; x < xdims[1]; x++)
     for(int y =0; y < xdims[2]; y++)
     for(int z =0; z <xdims[3]; z++){
					int index = w*xdims[1]*xdims[2]*xdims[3] + x*xdims[2]*xdims[3] + y*xdims[3] + z;
					if( (int) Xremapped[index] != 9)
     printf("w,x,y,z = %d,%d,%d,%d	index = %d    value is %d\n", w,x,y,z, index, (int) Xremapped[index] );
					}*/
    /*for(int w = 0; w < xdims[0]; w++)
     for(int z =0; z <xdims[3]; z++)
     for(int x = 0; x < xdims[1]; x++)
     for(int y =0; y < xdims[2]; y++){
					int outputindex = w*xdims[1]*xdims[2]*xdims[3] + z*xdims[1]*xdims[2] + x*xdims[2]+ y;
					int inputindex = w*xdims[1]*xdims[2]*xdims[3] + x*xdims[2]*xdims[3] + y*xdims[3] + z;
					if( (int) Xremapped[outputindex] != inputindex)
     printf("w,x,y,z = %d,%d,%d,%d	expectedindex = %d    value is %d\n", w,x,y,z, inputindex, (int) Xremapped[outputindex] );
					}*/
    
    
    //Unrolled table height is kernel^2*xdims[3] = 25 * no of channels
    //There will be batch_size (num_of_input_maps) number of tables
    //Unrolled table Width is number of possible kernel locations = (xdims[1] - kernel+1)^2
    //Each Thread Block should process 1 feature map ie 1 channel
    //No of blocks in X is (1*24*24)/1024
    //No of blocks in Y is 1
    
    
    /*	dim3 unrolBlock(1024, 1, 1);   //1024 threads in X direction
     int  unrol_x = ceil( xdims[3] * ydims[2]*ydims[1]/1024.0f ); //BlockIDx.x*blockDim.x+threadIdx.x is column number
     dim3 unrolGrid(unrol_x, 1, 1); //One block for each channel in each batch
     for(int batch = 0; batch < xdims[0]; batch++){
     n_unrolX_prof<<<unrolGrid,unrolBlock>>>(devXremapped, devXdims, batch, devX_unrol);
     }
     cudaDeviceSynchronize();*/
    
    
    /*float* X_unrolled = (float*) malloc(Xunrol_size);
     check_success(cudaMemcpy(X_unrolled, devX_unrol, Xunrol_size, cudaMemcpyDeviceToHost));
     for(int w = 0; w < xdims[0]; w++)
     for(int c = 0; c < xdims[3]; c++)
     for(int y = 0; y < 5*5; y++){
     for(int x = 0; x < ydims[1]*ydims[1]; x++){
					int inputindex = w*25*ydims[1]*ydims[1]*xdims[3] + c*25*ydims[1]*ydims[1] + y*ydims[1]*ydims[1] + x;
					//printf("%4d,", (int) X_unrolled[inputindex] );
					//if( X_unrolled[inputindex] != zoe_X_unrolled[inputindex])
					//	printf("%4d vs %4d ", (int) X_unrolled[inputindex], (int) zoe_X_unrolled[inputindex] );
     }
     }*/
    
    
    /* //Z-unroll matrix kernel
     //when we call this unroll kernel, we input all the x, including batch and channel, and output a whole unroll matrix, including batch, and channel
     //later when we do the matrix multiplication, we only have one big unroll matrix as matrix B.
     dim3 z_unrolGrid(xdims[3],xdims[0],1);  // one block for only an input map, xdims[3]= blockIdx.x means input map no. for one batch, blockIdx.y is batch size
     dim3 z_unrolBlock(xdims[1], xdims[2], 1);   // 2d block, x & y is input width and height,
     z_unrolX<<<z_unrolGrid,z_unrolBlock>>>(devX, devXdims, devX_unrol);
     cudaDeviceSynchronize();
     float* zoe_X_unrolled = (float*) malloc(Xunrol_size);
     check_success(cudaMemcpy(zoe_X_unrolled, devX_unrol, Xunrol_size, cudaMemcpyDeviceToHost));
     for(int w = 0; w < xdims[0]; w++)
     for(int y = 0; y < 5*5; y++){
     for(int x = 0; x < ydims[1]*ydims[2]; x++){
     int inputindex = w*25*ydims[1]*ydims[1] + y*ydims[1]*ydims[1] + x;
     if( X_unrolled[inputindex] != zoe_X_unrolled[inputindex])
					printf("%4d vs %4d ", (int) X_unrolled[inputindex], (int) zoe_X_unrolled[inputindex] );
     }
     printf("\n");
     }*/
    //Z-unroll remapped kernel,same grid dims, only use devXremapped
    dim3 z_unrolGrid(xdims[3],xdims[0],1);  // one block for only an input map, xdims[3]= blockIdx.x means input map no. for one batch, blockIdx.y is batch size
    dim3 z_unrolBlock(xdims[1], xdims[2], 1);   // 2d block, x & y is input width and height,
    z_unrolX_remapped<<<z_unrolGrid,z_unrolBlock>>>(devXremapped, devXdims, devX_unrol);
    cudaDeviceSynchronize();
    
    
    //total thread number is 25 * wdims[2]*wdims[3], one thead copy one element from W into unrollw
    dim3 z_unrollwGrid(wdims[3],1,1);
    dim3 z_unrollwBlock(5,5,wdims[2]); // at most 25*32<1024, legal
    z_unrollw<<<z_unrollwGrid, z_unrollwBlock>>>(devW, devW_unroll, devWdims);
    
    
    
    //Use Tiled multiplication
    int numARows = ydims[3];
    int numAColumns = (KERNEL_WIDTH*KERNEL_WIDTH*xdims[3]); //Not copying weights again
    int numBRows = (KERNEL_WIDTH*KERNEL_WIDTH*xdims[3]); 	//Took out xdims[0]);
    int numBColumns = ydims[1]*ydims[1];					//(xdims[1] - KERNEL_WIDTH+1)*(xdims[1] - KERNEL_WIDTH+1);
    int numCRows = ydims[3];
    int numCColumns = (xdims[1]-KERNEL_WIDTH+1)*(xdims[1]-KERNEL_WIDTH+1);
    int num_of_X_tiles = ceil(numBRows/32.0);
    //1 Thread processes 1 element in output table Y
    //Output columns = kernel^2 = 5*5
    //Output rows = number of output feature maps made for each digit = ydims[3]
    int x_blocks = ceil(numCColumns/32.0f);
    int y_blocks = ceil(numCRows/32.0f);
    dim3 z_DimGrid(x_blocks, y_blocks, 1);
    dim3 z_DimBlock(32, 32, 1);
    
    
    //copy devW_unroll back to cpu, then copy to constant memory.
    //when use this constant memory, only change multiply kernel
    if(Wsize<=(64*25*sizeof(float))){   //first convolution, and put all w into constant memory.
        float* cons_wunroll = (float*)malloc(Wsize);
        check_success(cudaMemcpy(cons_wunroll, devW_unroll, Wsize, cudaMemcpyDeviceToHost));
        check_success(cudaMemcpyToSymbol(dev_cons_Wunroll,cons_wunroll, Wsize));
        
        for(int batch =0; batch < ydims[0]; batch++){
            z_MatrixMultiply_perbatch_constant<<<z_DimGrid,z_DimBlock>>>(devX_unrol, devY,
                                                                         numARows, numAColumns,
                                                                         numBRows, numBColumns,
                                                                         numCRows, numCColumns,
                                                                         num_of_X_tiles, devYdims, devXdims,batch);
        }
    }
    else{//second convolution, only fisrt 29 rows of w canbe put into constant memory, the rest are still in the global memory
        for(int batch =0; batch < ydims[0]; batch++){
            z_MatrixMultiply_perbatch<<<z_DimGrid,z_DimBlock>>>(devW_unroll, devX_unrol, devY,
                                                                numARows, numAColumns,
                                                                numBRows, numBColumns,
                                                                numCRows, numCColumns,
                                                                num_of_X_tiles, devYdims, devXdims,batch);
        }
    }
    int relu4size = ceil( (Ysize/sizeof(float)) /1024.0f);
    dim3 grid(relu4size,1,1);
    dim3 block(1024,1,1);
    relu4_gpu<<<grid,block>>>(devY, Ysize/sizeof(float));
    
    
    
    
    /*dim3 DimGrid(x_blocks, y_blocks, 1);
     dim3 DimBlock(32, 32, 1);
     if(conv1d)
     MatrixMultiply<<<DimGrid,DimBlock>>>(constantW, devX_unrol, devY,
     numARows, numAColumns,
     numBRows, numBColumns,
     numCRows, numCColumns,
     num_of_X_tiles);
     else
     //    A, B, C, numARows, numAColumns, numBRows, numBColumns, numCRows, numCColumns
     MatrixMultiply<<<DimGrid,DimBlock>>>(devW, devX_unrol, devY,
     numARows, numAColumns,
     numBRows, numBColumns,
     numCRows, numCColumns,
     num_of_X_tiles);
     */
    
    
    //    device_query();
    //    conv_forward_kernel<<<DimGrid,DimBlock>>>(devX,devXdims,devW,conv1d, devY,devYdims);
    
    
    /* Original Serial Code
     
     //Total number of W filters is total input feature maps * total output feature maps
     for (int i =0; i < ydims[0]; i++ ) { //Number of input feature maps X
     for (int m =0; m < ydims[3]; m++ ) { //Number of output feature maps Y made for each digit
     for (int h =0; h < ydims[1]; h++ ) { //Height of output Y (=Input height - conv kernel height +1)
     for (int w =0; w < ydims[2]; w++ ) { //Width of ouptut Y (=Input width - conv kernel width +1)
     for (int p =0; p < filter_h; p++) { //Height of weight convolution kernel W
     for (int q =0; q < filter_w; q++) { //Width of weigth convolution kernel W
     for (int c =0; c < in_channel; c++) { //Each channel of W
     const auto yoffset = ((i * ydims[1] + h) * ydims[2] + w) * ydims[3] + m;
     const auto xoffset = i * xdims[1] * xdims[2] * xdims[3] +
     (h + p) * xdims[2] * xdims[3] +
     (w + q) * xdims[3] + c;
     const auto woffset = p * wdims[1] * wdims[2] * wdims[3] +
     q * wdims[2] * wdims[3] + c * wdims[3] + m;
     Y[yoffset] += X[xoffset] * W[woffset];
     }
     }
     }
     }
     }
     }
     }
     */
    cudaDeviceSynchronize();
    check_success(cudaFree(devX));
    check_success(cudaFree(devX_unrol));
    check_success(cudaFree(devXremapped));
    //check_success(cudaFree(devXdims));
    //check_success(cudaFree(devYdims));
    /*
     check_success(cudaMemcpy(Y, devY, Ysize, cudaMemcpyDeviceToHost));
     
     
     for(int c = 0; c < 1; c++){
     for(int y = 0; y < ydims[2]; y++){
     for(int x = 0; x < ydims[1]; x++){
     int index =  c * ydims[1] * ydims[2] + y * ydims[1] + x;
     printf("%f at location x,y,c = %d,%d,%d\n", Y[index],x,y,c );
     }
     }
     }*/
    
    const int pool_size = 2;
    //Updating old xdims and ydims, so they can be sent to average pool
    const int adims[] = {ydims[0], ydims[1], ydims[2], ydims[3] };
    const int bdims[] = {ydims[0], ydims[1]/pool_size, ydims[2]/pool_size, ydims[3] };
    //xdims[0] = ydims[0]; xdims[1] = ydims[1]; xdims[2] = ydims[2]; xdims[3] = ydims[3];
    //ydims[1] = ydims[1]/pool_size; ydims[2] = ydims[2]/pool_size; //No change to ydims[0] and ydims[3]
    const int Asize = adims[0]*adims[1]*adims[2]*adims[3]*sizeof(float);
    const int Bsize = bdims[0]*bdims[1]*bdims[2]*bdims[3]*sizeof(float);
    float* devB_after_pool;
    int* devBdims;
    check_success(cudaMalloc(&devB_after_pool,  Bsize));  //allocate space for afterpoolY
    check_success(cudaMalloc(&devBdims, sizeof(int)*4));
    check_success(cudaMemcpy(devBdims, &bdims[0], sizeof(int)*4, cudaMemcpyHostToDevice));
    dim3 avgPoolBlock(32,32,1);
    int avgpool_x_coal = ceil( bdims[2]/32.0f); // BlockIDx.x*blockDim.x+threadIdx.x is column number   //1
    int avgpool_y_coal = ceil( bdims[1]/32.0f); // BlockIDx.y*blockDim.y+threadIdx.y is row number    //1
    int avgpool_z_coal = ceil( bdims[3]/1.0f);  // BlockIDx.z*blockDim.z+threadIdx.z is channel   //32 then 64
    dim3 avgPoolGrid(avgpool_x_coal, avgpool_y_coal, avgpool_z_coal);
    for(int batch =0; batch < ydims[0]; batch++){
        n_averagepool_output_remapped<<<avgPoolGrid,avgPoolBlock>>>(devY, devYdims, pool_size,
                                                                    devB_after_pool, devBdims, batch);
    }
    cudaDeviceSynchronize();
    //check_success(cudaFree(devWdims));
    //Removed to test averagepool check_success(cudaMemcpy(Y, devY, Ysize, cudaMemcpyDeviceToHost));
    
    //check_success(cudaMemcpy(Y, devB_after_pool, Bsize, cudaMemcpyDeviceToHost));
    /* for(int c = 0; c < 1; c++){
     for(int y = 0; y < bdims[1]; y++){
     for(int x = 0; x < bdims[2]; x++){
     int index =  c * bdims[1] * bdims[2] + y * bdims[1] + x;
     printf("%f at location x,y,c = %d,%d,%d\n", Y[index],x,y,c );
     }
     }
     }
     */
    //Now start the second convolution, bdims now is used as xdims, wdims2 is wdims,
    //devB_after_pool used as devX, fisrt no need to remap X again
    //unroll X,
    
    //ydims2[] is set during function call
    float *devW2;
    int *devWdims2;
    int *devYdims2;
    float *devY2;
    float *devX_unrol2;
    float *devW_unroll2;
    int Wsize2 = sizeof(float) * KERNEL_WIDTH * KERNEL_WIDTH * wdims2[2] * wdims2[3];
    int Ysize2 = sizeof(float) * ydims2[1] * ydims2[2] * ydims2[3] * ydims2[0];
    int Xunrol_size2 = sizeof(float) * KERNEL_WIDTH * KERNEL_WIDTH *bdims[3] * bdims[0] * (bdims[1]-5+1)*(bdims[1]-5+1) ;
    check_success(cudaMalloc(&devW_unroll2, Wsize2));
    check_success(cudaMalloc(&devX_unrol2, Xunrol_size2));
    check_success(cudaMalloc(&devWdims2, sizeof(int)*4));
    check_success(cudaMalloc(&devW2, Wsize2));
    check_success(cudaMalloc(&devYdims2, sizeof(int)*4));
    check_success(cudaMalloc(&devY2, Ysize2));
    check_success(cudaMemcpy(devWdims2, &wdims2[0], sizeof(int)*4, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devW2, W2, Wsize2, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devYdims2, &ydims2[0], sizeof(int)*4, cudaMemcpyHostToDevice));
    
    
    dim3 z_unrolGrid2(bdims[3],bdims[0],1);  // one block for only an input map, xdims[3]= blockIdx.x means input map no. for one batch, blockIdx.y is batch size
    dim3 z_unrolBlock2(bdims[1], bdims[2], 1);   // 2d block, x & y is input width and height,
    z_unrolX_remapped<<<z_unrolGrid2,z_unrolBlock2>>>(devB_after_pool, devBdims, devX_unrol2);
    cudaDeviceSynchronize();
    
    dim3 z_unrollwGrid2(wdims2[3],1,1);
    dim3 z_unrollwBlock2(5,5,wdims2[2]); // at most 25*32<1024, legal
    z_unrollw<<<z_unrollwGrid2, z_unrollwBlock2>>>(devW2, devW_unroll2, devWdims2);
    
    numARows = ydims2[3];
    numAColumns = (KERNEL_WIDTH*KERNEL_WIDTH*bdims[3]); //Not copying weights again
    numBRows = (KERNEL_WIDTH*KERNEL_WIDTH*bdims[3]);  //Took out xdims[0]);
    numBColumns = ydims2[1]*ydims2[1];          //(xdims[1] - KERNEL_WIDTH+1)*(xdims[1] - KERNEL_WIDTH+1);
    numCRows = ydims2[3];
    numCColumns = (bdims[1]-KERNEL_WIDTH+1)*(bdims[1]-KERNEL_WIDTH+1);
    num_of_X_tiles = ceil(numBRows/32.0);
    //1 Thread processes 1 element in output table Y
    //Output columns = kernel^2 = 5*5
    //Output rows = number of output feature maps made for each digit = ydims[3]
    x_blocks = ceil(numCColumns/32.0f);
    y_blocks = ceil(numCRows/32.0f);
    dim3 z_DimGrid2(x_blocks, y_blocks, 1);
    dim3 z_DimBlock2(32, 32, 1);
    
    
    //copy devW_unroll back to cpu, then copy to constant memory.
    //when use this constant memory, only change multiply kernel
    if(Wsize2<=(64*25*sizeof(float))){   //first convolution, and put all w into constant memory.
        float* cons_wunroll = (float*)malloc(Wsize);
        check_success(cudaMemcpy(cons_wunroll, devW_unroll, Wsize, cudaMemcpyDeviceToHost));
        check_success(cudaMemcpyToSymbol(dev_cons_Wunroll,cons_wunroll, Wsize));
        
        for(int batch =0; batch < ydims[0]; batch++){
            z_MatrixMultiply_perbatch_constant<<<z_DimGrid,z_DimBlock>>>(devX_unrol, devY,
                                                                         numARows, numAColumns,
                                                                         numBRows, numBColumns,
                                                                         numCRows, numCColumns,
                                                                         num_of_X_tiles, devYdims, devXdims,batch);
        }
    }
    else{//second convolution, only fisrt 29 rows of w canbe put into constant memory, the rest are still in the global memory
        for(int batch =0; batch < ydims2[0]; batch++){
            z_MatrixMultiply_perbatch<<<z_DimGrid2,z_DimBlock2>>>(devW_unroll2, devX_unrol2, devY2,
                                                                  numARows, numAColumns,
                                                                  numBRows, numBColumns,
                                                                  numCRows, numCColumns,
                                                                  num_of_X_tiles, devYdims2, devBdims,batch);
        }
    }
    relu4size = ceil( (Ysize2/sizeof(float)) /1024.0f);
    dim3 grid2(relu4size,1,1);
    relu4_gpu<<<grid2,block>>>(devY2, Ysize2/sizeof(float));

    cudaDeviceSynchronize();
    
    
    //Updating old xdims and ydims, so they can be sent to average pool
    // const int adims[] = {ydims[0], ydims[1], ydims[2], ydims[3] };
    const int bdims2[] = {ydims2[0], ydims2[1]/pool_size, ydims2[2]/pool_size, ydims2[3] };
    
    // const int Asize = adims[0]*adims[1]*adims[2]*adims[3]*sizeof(float);
    const int Bsize2 = bdims2[0]*bdims2[1]*bdims2[2]*bdims2[3]*sizeof(float);
    float* devB_after_pool2;
    int* devBdims2;
    check_success(cudaMalloc(&devB_after_pool2,  Bsize2));  //allocate space for afterpoolY
    check_success(cudaMalloc(&devBdims2, sizeof(int)*4));
    check_success(cudaMemcpy(devBdims2, &bdims2[0], sizeof(int)*4, cudaMemcpyHostToDevice));
    dim3 avgPoolBlock2(32,32,1);
    avgpool_x_coal = ceil( bdims2[2]/32.0f); // BlockIDx.x*blockDim.x+threadIdx.x is column number   //1
    avgpool_y_coal = ceil( bdims2[1]/32.0f); // BlockIDx.y*blockDim.y+threadIdx.y is row number    //1
    avgpool_z_coal = ceil( bdims2[3]/1.0f);  // BlockIDx.z*blockDim.z+threadIdx.z is channel   //32 then 64
    dim3 avgPoolGrid2(avgpool_x_coal, avgpool_y_coal, avgpool_z_coal);
    for(int batch =0; batch < ydims2[0]; batch++){
        n_averagepool_coalesced<<<avgPoolGrid2,avgPoolBlock2>>>(devY2, devYdims2, pool_size,
                                                                devB_after_pool2, devBdims2, batch);
    }
    cudaDeviceSynchronize();
    
    
    
    
    
    
    //cudaDeviceReset();
    //printf("5555");
    /*  check_success(cudaMemcpy(Y, devY, Ysize, cudaMemcpyDeviceToHost));
     
     
     for(int c = 0; c < 1; c++){
     for(int y = 0; y < ydims[2]; y++){
     for(int x = 0; x < ydims[1]; x++){
     int index =  c * ydims[1] * ydims[2] + y * ydims[1] + x;
     printf("%f at location x,y,c = %d,%d,%d\n", Y[index],x,y,c );
     }
     }
     }  */
    
    check_success(cudaMemcpy(Y, devB_after_pool2, Bsize2, cudaMemcpyDeviceToHost));
    check_success(cudaFree(devX));
    //printf("3333");
    check_success(cudaFree(devW));
    check_success(cudaFree(devY));
    check_success(cudaFree(devXdims));
    check_success(cudaFree(devYdims));
    check_success(cudaFree(devWdims));
}


static void fully_forward_gpu(const float *X, const int xdims[2], float *W,
                                 const int wdims[2], float *Y, const int ydims[2]){
    float *devX;
    float *devW;
    float *devY;
    int *devXdims;
    int *devWdims;
    int *devYdims;
    int Xsize = xdims[0]*xdims[1]*sizeof(float);
    int Wsize = wdims[0]*wdims[1]*sizeof(float);
    int Ysize = ydims[0]*ydims[1]*sizeof(float);
    check_success(cudaMalloc(&devX, Xsize));
    check_success(cudaMalloc(&devY, Ysize));
    check_success(cudaMalloc(&devW, Wsize));
    
    check_success(cudaMalloc(&devYdims, sizeof(int)*2));
    check_success(cudaMalloc(&devWdims, sizeof(int)*2));
    
    check_success(cudaMalloc(&devXdims, sizeof(int)*2));
    
    check_success(cudaMemcpy(devX, X, Xsize, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devW, W, Wsize, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devXdims, &xdims[0], sizeof(int)*2, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devWdims, &wdims[0], sizeof(int)*2, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devYdims, &ydims[0], sizeof(int)*2, cudaMemcpyHostToDevice));
    /*  for(int i =0;i< 100;i++){
     printf("%f  ", X[i]);
     }*/
    dim3 fullyforwardGrid(ceil(ydims[0]*ydims[1]/1024.0),1,1); //i =(threadIdx.x + blockIdx.x * blockDim.x)/wdims[1];
    dim3 fullyforwardBlock(1024,1,1); //j = (threadIdx.x + blockIdx.x * blockDim.x)%wdims[1];
    fullyforward_kernel<<<fullyforwardGrid, fullyforwardBlock>>>(devX, devXdims, devW, devWdims, devY, devYdims);
    
    check_success(cudaMemcpy(Y, devY, Ysize, cudaMemcpyDeviceToHost));
    /*for(int i = 0; i< ydims[0];i++){
     for ( int j =0; j< ydims[1];j++){
     printf("%f(%d) ", Y[i*ydims[1]+j], i*ydims[1]+j);
     }
     printf("\n");
     }*/
    check_success(cudaFree(devX));
    check_success(cudaFree(devW));
    check_success(cudaFree(devY));
    check_success(cudaFree(devXdims));
    check_success(cudaFree(devYdims));
    check_success(cudaFree(devWdims));
}

void relu2_gpu(float *X, const int Ndims[2]) {
    for (const auto i : range(0, Ndims[0] * Ndims[1])) {
        X[i] = (X[i] < 0) ? 0 : X[i];
    }
}

// Choose the guess with largest score
void argmax_gpu(const float *X, const int Ndims[2], int *Y) {
    for (const auto i : range(0, Ndims[0])) {
        auto max_idx = 0;
        auto max     = X[i * Ndims[1]];
        for (const auto j : range(0, Ndims[1])) {
            const auto elem = X[(i * Ndims[1]) + j];
            if (elem > max) {
                max_idx = j;
                max     = elem;
            }
        }
        Y[i] = max_idx;
    }
}



float* forward_operation_gpu(float *x, float *conv1, float *conv2, float *fc1, float *fc2, int *out) {
    // conv1 layer
    //const shape adims = {xdims.num, conv1dims.num, (xdims.height - conv1dims.height + 1),
    //    (xdims.width - conv1dims.width + 1)};
    const int adims[] = {xdims_g[0], (xdims_g[1] - conv1dims_g[0] + 1),
                        (xdims_g[2] - conv1dims_g[1] + 1), conv1dims_g[3]};
    auto a            = zeros<float>(adims);
    //conv_forward_valid(x, xdims, conv1, conv1dims, a, adims);
    
    // relu layer
    //relu4(a, adims);
    
    // sub-sampling: average pooling
    const int pool_size = 2;
    //const shape bdims   = {adims.num, adims.depth, adims.height / pool_size, adims.width / pool_size};
    const int bdims[]   = {adims[0], adims[1] / pool_size, adims[2] / pool_size,
                            adims[3]};
    auto b              = zeros<float>(bdims);
    //average_pool(a, adims, pool_size, b, bdims);
    
    // conv2 layer
    //const shape cdims = {bdims.num, conv2dims.num, (bdims.height - conv2dims.height + 1),
    //    (bdims.width - conv2dims.width + 1)};
    const int cdims[] = {bdims[0], (bdims[1] - conv2dims_g[0] + 1),
                        (bdims[2] - conv2dims_g[1] + 1), conv2dims_g[3]};
    auto c            = zeros<float>(cdims);
    //conv_forward_valid(b, bdims, conv2, conv2dims, c, cdims);
    conv_forward_valid_gpu (x,xdims_g,conv1,conv1dims_g,conv2, conv2dims_g,c,adims, cdims);
    
    // relu
    //relu4(c, cdims);
    
    // sub-sampling: average pooling
    //const shape ddims = {cdims.num, cdims.depth, cdims.height / pool_size, cdims.width / pool_size};
    const int ddims[] = {cdims[0], cdims[1] / pool_size, cdims[2] / pool_size, cdims[3]};
    auto d            = zeros<float>(ddims);
    //average_pool(c, cdims, pool_size, d, ddims);
    
    // reshape
    //const shape ddims2 = {ddims.num, ddims.depth * ddims.height * ddims.width};
    const int ddims2[] = {ddims[0], ddims[1] * ddims[2] * ddims[3]};
    
    // fully connected layer 1: matrix multiplication
    //const shape edims = {ddims.num, fc1dims.width};
    const int edims[] = {ddims[0], fc1dims_g[1]};
    auto e            = zeros<float>(edims);
    //fully_forward(d, ddims2, fc1, fc1dims, e, edims);
    fully_forward_gpu(c, ddims2, fc1, fc1dims_g, e, edims);
    
    // relu
    relu2_gpu(e, edims);
    
    // fully connected layer 2: matrix multiplication
    //const shape fdims = {edims.height, fc2dims.width};
    const int fdims[] = {edims[0], fc2dims_g[1]};
    auto f            = zeros<float>(fdims);
    //fully_forward(e, edims, fc2, fc2dims, f, fdims);
    fully_forward_gpu(e, edims, fc2, fc2dims_g, f, fdims);

    std::cout << fdims[0]<<","<< fdims[1]<<"\n";
    argmax_gpu(f, fdims, out);
    
    delete[] a;
    delete[] b;
    delete[] c;
    delete[] d;
    delete[] e;
    return f;// delete[] f;
}



//28*28 threads per block all working together on each convolution
//1000 blocks in one grid, because 1000 batches.
__global__ void gpu_convolution_DEDX(const float* dy, float* dev_DEDX, const float* devW){
    int batch = blockIdx.x;
    int h = threadIdx.y;
    int w = threadIdx.x;
    const int ysize = 20*24*24;
    int i = threadIdx.y * 28 + threadIdx.x;
    int totalt = 28*28;
    __shared__ float sharedy[ysize];
    for( ; i < ysize; i += totalt ){
        if(i < ysize){
            sharedy[i] = dy[batch*32*24*24 + i];
        }
    }
    __syncthreads();
    float temp = 0;
    float dyvalue;
    /*const int Wsize = 32*5*5;
    __shared__ float shared_W[Wsize];
    for( ;i < Wsize; i ++ ){
        if(i < Wsize){
            shared_W[i] = W[];
        }
    }*/
    
    for(int c = 0; c < 32; c++){
        for(int p = 0; p < 5; p++){
            for(int q = 0; q < 5; q++){
                if(((h-p)>=24) || ((h-p) <0) || ((w-q)<0) || ((w-q)>=24)){
                    dyvalue = 0;
                }
                else if(c<20){
                    dyvalue = sharedy[c*24*24 + (h-p)*24 + (w-q)];
                }
                else{
                    dyvalue = dy[batch * 24*24*32 + c*24*24+(h-p)*24+(w-q)];
                }
                temp += dyvalue *  devW[c*25+p*5 +q];
            }
        }
    }
    dev_DEDX[batch*totalt + h * 28 + w] = temp;
}

float* gpu_backward(float *x, float *conv1, const float *y1, float *dedy, float *dedw, float *dedx){
    float *test = (float*)malloc(1000*32*24*24);
    float *input_device;
    float *conv1_device;
    float *input_unroll_device;
    float *tunroll;
    float *output1_device;
    float *y1_device;
    float *dy;
    float *dw;
    float *dx;
    cudaMalloc(&conv1_device, sizeof(float)*25*32);
    cudaMalloc(&input_device,sizeof(float)*1000*28*28);
    cudaMalloc(&input_unroll_device, sizeof(float)*1000*25*24*24);
    cudaMalloc(&tunroll,sizeof(float)*1000*25*24*24);
    cudaMalloc(&output1_device, sizeof(float)*1000*32*24*24);
    cudaMalloc(&y1_device, sizeof(float)*1000*32*24*24);
    cudaMalloc(&dy, sizeof(float)*1000*32*24*24);
    cudaMalloc(&dw, sizeof(float)*32*25);
    cudaMalloc(&dx, sizeof(float)*28*28*1000);
    cudaMemcpy(input_device,x,1000*28*28*sizeof(float),cudaMemcpyHostToDevice);
    cudaMemcpy(conv1_device, conv1, 25 * 32*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(y1_device, y1, 1000 * 32 * 24 * 24*sizeof(float), cudaMemcpyHostToDevice);
    
    //copy to constant
    cudaMemcpyToSymbol(dev_cons_Wunroll,conv1, sizeof(float)*25*32);
    
    //launch unroll x
    dim3 grid(1000,1,1);
    dim3 block(512,1,1);
    unroll<<<grid,block>>>(input_device, input_unroll_device, tunroll);
    //408 method
    static int ydims1[] = {1000,24,24,32};
    static int xdims1[] = {1000,5,5,1};
    int *devYdims;
    int *devXdims;
    cudaMalloc(&devYdims, sizeof(int)*4);
    cudaMalloc(&devXdims, sizeof(int)*4);
    cudaMemcpy(devYdims, &ydims1[0], sizeof(int)*4, cudaMemcpyHostToDevice);
    cudaMemcpy(devXdims, &xdims1[0], sizeof(int)*4, cudaMemcpyHostToDevice);
    int num_of_X_tiles = ceil(25/32.0);
    int x_blocks = ceil(576/32.0f);
    int y_blocks = ceil(32/32.0f);
    dim3 z_DimGrid(x_blocks, y_blocks, 1);
    dim3 z_DimBlock(32, 32, 1);
    
    //constant w
    for(int batch =0; batch < 1000; batch++){
        z_MatrixMultiply_perbatch_constant<<<z_DimGrid,z_DimBlock>>>(input_unroll_device, output1_device,
                                                                     32, 25,
                                                                     25, 576,
                                                                     32, 576,
                                                                     num_of_X_tiles, devYdims, devXdims,batch);
    }
    //copy back
    const shape ydims = {xdims.num, conv1dims.num, (xdims.height - conv1dims.height + 1),
        (xdims.width - conv1dims.width + 1)};
    
    //dedy
    dim3 grid_dim_dy(1000,1,1);
    dim3 block_dim_dy(576,1,1);
    gpu_backward_ygrad<<<grid_dim_dy,block_dim_dy>>>(y1_device, output1_device);
    
    //dedw
    num_of_X_tiles = ceil(576/32.0);
    x_blocks = ceil(25/32.0f);
    y_blocks = ceil(32/32.0f);
    dim3 dw_DimGrid(x_blocks, y_blocks, 1);
    dim3 dw_DimBlock(32, 32, 1);
    
    
    for(int batch = 0; batch <1; batch ++){
        gpu_backward_dw<<<dw_DimGrid,dw_DimBlock>>>(output1_device,tunroll,dw,32,576,576,25,32,25,num_of_X_tiles,batch);
    }
    //copy back
    cudaMemcpy(dedy, output1_device, sizeof(float)*32*1000*24*24, cudaMemcpyDeviceToHost);
    cudaMemcpy(dedw, dw, sizeof(float)*32*25, cudaMemcpyDeviceToHost);
    return dw;/*
    //dedx
    dim3 grid_dim_dx(1000,1,1);
    dim3 block_dim_dx(28,28,1);
    gpu_backward_dx<<<grid_dim_dx,block_dim_dx>>>(output1_device,dx);
    cudaMemcpy(dedx,dx,sizeof(float)*28*1000*28, cudaMemcpyDeviceToHost);*/
}

static void compare_solution2(float *cpu, const int cpu_size, float *gpu, const int gpu_size) {
    const float tolerance = 0.01f;
    if (cpu_size != gpu_size) {
        std::cout << "The dimensions do not match." << cpu_size <<" vs "<< gpu_size <<"\n";
        return;
    }
    std::cout << "Comparing " << cpu_size << " elements.\n";
    // element-wise comparison: only prints out the first error and halts
    for (const auto i : range(0, cpu_size)) {
        if (std::abs(cpu[i] - gpu[i]) > tolerance) {
            std::cout << "Element " << i << " does not match.\n";
            std::cout << "cpu="<< cpu[i] << ",gpu=" << gpu[i] <<"\n";
            //return;
        }
    }
    std::cout << "All the elements match!\n";
}






//static void conv_forward_valid_gpu (const float *X, const int xdims[4],
//                                    const float *W, int wdims[4], const float *W2,
//                                    int wdims2[4], float *Y, const int ydims[4], const int ydims2[4]) {
static float* conv_backward_DEDW_n_DEDX_gpu(const float *devX, float *devX_unrol, const int xdims[4],
                                    const float *dev_DEDY, const int wdims[4],
                                    float *dev_DEDW, const int ydims[4], float* dev_DEDX, const float *devW ){
    
    float *devXremapped;
    //float *devX_unrol;
    //same as dev_DEDY float *devW;
    float *devW_unroll;
    //float *devY;
    int *devXdims;
    int *devWdims;
    int *devYdims;
    const int Wsize = sizeof(float) * wdims[0] * wdims[1] * wdims[2] * wdims[3];
    const int Xsize = sizeof(float) * xdims[0] * xdims[1] * xdims[2] * xdims[3];
    //const int Xunrol_size = sizeof(float) * KERNEL_WIDTH * KERNEL_WIDTH *xdims[3] * xdims[0] * (xdims[1]-5+1)*(xdims[1]-5+1) ;
    const int Xunrol_size = sizeof(float) * wdims[1] * wdims[1] * xdims[3] * xdims[0] * (xdims[1]-wdims[1]+1)*(xdims[1]-wdims[1]+1) ;
    const int Ysize = sizeof(float) * ydims[1] * ydims[2] * ydims[3] * ydims[0];
    printf("DEDYdims[0]=%d\n",     wdims[0]);
    printf("DEDYdims[1]=%d\n",     wdims[1]);
    printf("DEDYdims[2]=%d\n",     wdims[2]);
    printf("DEDYdims[3]=%d\n",     wdims[3]);
    printf("Ydims[0]=%d\n",     ydims[0]);
    printf("Ydims[1]=%d\n",     ydims[1]);
    printf("Ydims[2]=%d\n",     ydims[2]);
    printf("Ydims[3]=%d\n",     ydims[3]);
    printf("Wsize=%d\n",        Wsize);
    printf("Xsize=%d\n",        Xsize);
    printf("Xunrol_size=%d\n",  Xunrol_size);
    printf("Ysize=%d\n",        Ysize);

    check_success(cudaMalloc(&devXremapped, Xsize));
    //check_success(cudaMalloc(&devX_unrol, Xunrol_size));
    //check_success(cudaMalloc(&devW, Wsize));
    check_success(cudaMalloc(&devW_unroll, Wsize));     // new added to change the way filters store in memory
    check_success(cudaMalloc(&devWdims, sizeof(int)*4));
    //check_success(cudaMalloc(&devY, Ysize));
    check_success(cudaMalloc(&devXdims, sizeof(int)*4));
    check_success(cudaMalloc(&devYdims, sizeof(int)*4));
    
    dim3 ReMapBlock(32,32,1);
    //Each Thread block processes all channels in 28 and 28 image
    int Remap_x = ceil( xdims[2]/32.0f); // BlockIDx.x*blockDim.x+threadIdx.x is column number //1
    int Remap_y = ceil( xdims[1]/32.0f); // BlockIDx.y*blockDim.y+threadIdx.y is row number    //1
    int Remap_z = ceil( xdims[0]/1.0f);  // BlockIDx.z*blockDim.z+threadIdx.z is batch number  //10
    dim3 ReMapGrid(Remap_x, Remap_y, Remap_z);
    //n_RemapX<<<ReMapGrid, ReMapBlock>>>(devX, devXdims, devXremapped);
    
    //Z-unroll remapped kernel,same grid dims, only use devXremapped
    dim3 z_unrolGrid(xdims[3],xdims[0],1);  // one block for only an input map, xdims[3]= blockIdx.x means input map no. for one batch, blockIdx.y is batch size
    dim3 z_unrolBlock(xdims[1], xdims[2], 1);   // 2d block, x & y is input width and height,
    //z_unrolX_remapped<<<z_unrolGrid,z_unrolBlock>>>(devXremapped, devXdims, devX_unrol);
    cudaDeviceSynchronize();
    
    
    //total thread number is 25 * wdims[2]*wdims[3], one thead copy one element from W into unrollw
    dim3 z_unrollwGrid(wdims[3],1,1);
    dim3 z_unrollwBlock(wdims[1], wdims[1], wdims[2]); // at most 25*32<1024, legal
    //z_unrollw<<<z_unrollwGrid, z_unrollwBlock>>>(dev_DEDY, devW_unroll, devWdims);
    
    
    //Use Tiled multiplication
    int numARows = ydims[3];
    int numAColumns = (wdims[1]*wdims[1]*xdims[3]);
    int numBRows = (wdims[1]*wdims[1]*xdims[3]); 	//Took out xdims[0]);
    int numBColumns = ydims[1]*ydims[1];					//(xdims[1] - KERNEL_WIDTH+1)*(xdims[1] - KERNEL_WIDTH+1);
    int numCRows = ydims[3];
    int numCColumns = (xdims[1]-wdims[1]+1)*(xdims[1]-wdims[1]+1);
    printf("numARows=%d\n",     numARows);
    printf("numAColumns=%d\n",  numAColumns);
    printf("numBRows=%d\n",     numBRows);
    printf("numBColumns=%d\n",  numBColumns);
    printf("numCRows=%d\n",     numCRows);
    printf("numCColumns=%d\n",  numCColumns);
    
    //1 Thread processes 1 element in output table Y
    //Output columns = kernel^2 = 5*5
    //Output rows = number of output feature maps made for each digit = ydims[3]
    int num_of_X_tiles = ceil(numBRows/32.0);
    int x_blocks = ceil(numCColumns/32.0f);
    int y_blocks = ceil(numCRows/32.0f);
    dim3 z_DimGrid(x_blocks, y_blocks, 1);
    dim3 z_DimBlock(32, 32, 1);
    
    
    //copy devW_unroll back to cpu, then copy to constant memory.
    //when use this constant memory, only change multiply kernel
    /*if(Wsize<=(64*25*sizeof(float))){   //first convolution, and put all w into constant memory.
        float* cons_wunroll = (float*)malloc(Wsize);
        check_success(cudaMemcpy(cons_wunroll, devW_unroll, Wsize, cudaMemcpyDeviceToHost));
        check_success(cudaMemcpyToSymbol(dev_cons_Wunroll,cons_wunroll, Wsize));
        
        for(int batch =0; batch < ydims[0]; batch++){
            z_MatrixMultiply_perbatch_constant<<<z_DimGrid,z_DimBlock>>>(devX_unrol, dev_DEDW,
                                                                         numARows, numAColumns,
                                                                         numBRows, numBColumns,
                                                                         numCRows, numCColumns,
                                                                         num_of_X_tiles, devYdims, devXdims,batch);
        }
    }
    else{*/
    std::cout << Wsize;
        //for(int batch =0; batch < wdims[0]; batch++){
        for(int batch =0; batch < 1; batch++){
            z_MatrixMultiply_DEDW<<<z_DimGrid,z_DimBlock>>>(dev_DEDY, devX_unrol, dev_DEDW,
                                                                numARows, numAColumns,
                                                                numBRows, numBColumns,
                                                                numCRows, numCColumns,
                                                                num_of_X_tiles, batch); //devYdims, devXdims,
        }
        //check_success(cudaDeviceSynchronize());
        
    //}
    
    printf("DEDW completed\n");
    dim3 grid_DEDX(1000,1,1);
    dim3 block_DEDX(28,28,1);
    gpu_convolution_DEDX<<<grid_DEDX,block_DEDX>>>(dev_DEDY,dev_DEDX, devW);
    check_success(cudaDeviceSynchronize());
    printf("DEDX completed\n");
    return dev_DEDW;

    check_success(cudaFree(devXremapped));
    //check_success(cudaFree(devX_unrol));
    check_success(cudaFree(devW_unroll));
    check_success(cudaFree(devWdims));
    check_success(cudaFree(devXdims));
    check_success(cudaFree(devYdims));
}



/******************************************************************************
 Sequential Functions
*******************************************************************************/
static void generate_data(float *x, const shape &xdims) {
  // input dimension size
  std::cout << "generating tensor with input dimensions = " << xdims.num << " x " << xdims.depth << " x "
            << xdims.height << " x " << xdims.width << "\n";

  const auto rng_state = rng_new_state();

  for (const auto ii : range(0, xdims.flattened_length())) {
    x[ii] = rng_float(rng_state);
  }

  delete rng_state;
}

// generate convolution filter
static void generate_convfilters(float *conv, const shape &convdim) {
  // convolution filter dimension size
  std::cout << "filter dimensions = " << convdim.num << " x " << convdim.depth << " x " << convdim.height << " x "
            << convdim.width << "\n";

  // Set convolution filter values to 1
  std::fill(conv, conv + convdim.flattened_length(), 1);
}

// Rectified linear unit 4d
static void relu4(float *X, const shape &xdims) {
  for (const auto i : range(0, xdims.flattened_length())) {
    X[i] = (X[i] < 0) ? 0 : X[i];
  }
}

// Rectified linear unit 2d
static void relu2(float *X, const shape &xdims) {
  for (const auto i : range(0, xdims.height * xdims.width)) {
    X[i] = (X[i] < 0) ? 0 : X[i];
  }
}

// From book chapter Figure 16.5
static void average_pool(const float *X, const shape &xdims, const int pool_size, float *Y, const shape &ydims) {
  const auto scale = 1.0f / static_cast<float>(pool_size * pool_size);
  for (const auto i : range(0, ydims.num)) {
    for (const auto m : range(0, ydims.depth)) {
      for (const auto h : range(0, ydims.height)) {
        for (const auto w : range(0, ydims.width)) {
          const auto yoffset = ((i * ydims.depth + m) * ydims.height + h) * ydims.width + w;
          for (const auto p : range(0, pool_size)) {
            for (const auto q : range(0, pool_size)) {
              const auto xoffset =
                  ((((i * xdims.depth) + m) * xdims.height) + (pool_size * h + p)) * xdims.width + (pool_size * w + q);
              Y[yoffset] += X[xoffset] * scale;
            }
          }
        }
      }
    }
  }
}


// Choose the guess with largest score
static void argmax(const float *X, const shape &xdims, int *Y) {
  for (const auto i : range(0, xdims.height)) {
    auto max_idx = 0;
    auto max     = X[i * xdims.width];
    for (const auto j : range(0, xdims.width)) {
      const auto elem = X[(i * xdims.width) + j];
      if (elem > max) {
        max_idx = j;
        max     = elem;
      }
    }
    Y[i] = max_idx;
  }
}

static void print_array(const float *data, const shape &dim) {
  std::cout << "Printing array\n";
  for (const auto i : range(0, dim.flattened_length())) {
    std::cout << (float) data[i] << " ";
  }
  std::cout << std::endl;
}

// From book chapter Figure 16.4
// Sequential code for the forward path of the convolution layer
static void conv_forward_valid(const float *X, const shape &xdims, const float *W, const shape &wdims, float *Y,
                               const shape &ydims) {
  std::fill(Y, Y + ydims.flattened_length(), 0);

  for (const auto i : range(0, ydims.num)) {       //for whole batch
    for (const auto m : range(0, ydims.depth)) {    // for each output feature map
      for (const auto h : range(0, ydims.height)) { // for each output element
        for (const auto w : range(0, ydims.width)) {
          const auto yoffset = ((i * ydims.depth + m) * ydims.height + h) * ydims.width + w;
          for (const auto c : range(0, xdims.depth)) {     // sum over all input feature maps
            for (const auto p : range(0, wdims.height)) {  // filter height
              for (const auto q : range(0, wdims.width)) { // filter width
                const auto xoffset = ((((i * xdims.depth) + c) * xdims.height) + (h + p)) * xdims.width + (w + q);
                const auto woffset = ((((m * wdims.depth) + c) * wdims.height) + p) * wdims.width + q;
                Y[yoffset] += X[xoffset] * W[woffset];
              }
            }
          }
        }
      }
    }
  }
}

void fully_forward(const float *X, const shape &xdims, float *W, const shape &wdims, float *Y, const shape &ydims) {
  for (const auto i : range(0, xdims.height)) {
    for (const auto j : range(0, wdims.width)) {
      float sum = 0;
      for (const auto k : range(0, xdims.width)) {
        sum += X[i * xdims.width + k] * W[k * wdims.width + j];
      }
      Y[i * wdims.width + j] = sum;
    }
  }
}

// error gradient of computed y respect to the original/correct y value
static void conv_backward_ygrad(const float *Y_orig, const float *Y, const shape &ydims, float *dE_dY) {
  for (const auto i : range(0, ydims.num)) {
    for (const auto m : range(0, ydims.depth)) {    // for each output feature map
      for (const auto h : range(0, ydims.height)) { // for each output element
        for (const auto w : range(0, ydims.width)) {
          const auto yoffset = ((i * ydims.depth + m) * ydims.height + h) * ydims.width + w;
          dE_dY[yoffset]     = Y[yoffset] - Y_orig[yoffset];
        }
      }
    }
  }
}

// backward propagation for dE/dW
static void conv_backward_wgrad(const float *X, const shape &xdims, const float *W, const shape &wdims,
                                const shape &ydims, const float *dE_dY, float *dE_dW) {
  std::fill(dE_dW, dE_dW + (ydims.depth * wdims.height * wdims.width * wdims.depth), 0);

  for (const auto i : range(0, 1)){//ydims.num // if set to 1, then can have tolerance of 0.01f
    for (const auto m : range(0, ydims.depth)) {    // for each output feature map
      for (const auto h : range(0, ydims.height)) { // for each output element
        for (const auto w : range(0, ydims.width)) {
          for (const auto c : range(0, wdims.depth)) {     // sum over all input feature maps
            for (const auto p : range(0, wdims.height)) {  // filter height
              for (const auto q : range(0, wdims.width)) { // filter width
                const auto yoffset = ((i * ydims.depth + m) * ydims.height + h) * ydims.width + w;
                const auto xoffset = ((((i * xdims.depth) + c) * xdims.height) + (h + p)) * xdims.width + (w + q);
                const auto woffset = ((((m * wdims.depth) + c) * wdims.height) + p) * wdims.width + q;
                dE_dW[woffset] += X[xoffset] * dE_dY[yoffset];
              }
            }
          }
        }
      }
    }
  }
}

// backward propagation for dE/dX
static void conv_backward_xgrad(const float *X, const shape &xdims, const float *W, const shape &wdims,
                                const shape &ydims, const float *dE_dY, float *dE_dX) {

  std::fill(dE_dX, dE_dX + (ydims.num * ydims.depth * ydims.height * wdims.depth), 0);

  for (const auto i : range(0, ydims.num)) {
    for (const auto m : range(0, ydims.depth)) {    // for each output feature map
      for (const auto h : range(0, ydims.height)) { // for each output element
        for (const auto w : range(0, ydims.width)) {
          for (const auto c : range(0, xdims.depth)) {     // sum over all input feature maps
            for (const auto p : range(0, wdims.height)) {  // filter height
              for (const auto q : range(0, wdims.width)) { // filter width
                const auto yoffset = ((i * ydims.depth + m) * ydims.height + h) * ydims.width + w;
                const auto xoffset = ((((i * xdims.depth) + c) * xdims.height) + (h + p)) * xdims.width + (w + q);
                const auto woffset = ((((m * wdims.depth) + c) * wdims.height) + p) * wdims.width + q;
                dE_dX[xoffset] += dE_dY[yoffset] * W[woffset];
              }
            }
          }
        }
      }
    }
  }
}



// Forward operation for the CNN, a combination of conv layer + relu + average pooling
float* forward_operation_cpu(float *x, float *conv1, float *conv2, float *fc1, float *fc2, int *out) {
  // conv1 layer
  const shape adims = {xdims.num, conv1dims.num, (xdims.height - conv1dims.height + 1),
                       (xdims.width - conv1dims.width + 1)};
  auto a            = zeros<float>(adims);
  conv_forward_valid(x, xdims, conv1, conv1dims, a, adims);

  // relu layer
  relu4(a, adims);

  // sub-sampling: average pooling
  const int pool_size = 2;
  const shape bdims   = {adims.num, adims.depth, adims.height / pool_size, adims.width / pool_size};
  auto b              = zeros<float>(bdims);
  average_pool(a, adims, pool_size, b, bdims);

  // conv2 layer
  const shape cdims = {bdims.num, conv2dims.num, (bdims.height - conv2dims.height + 1),
                       (bdims.width - conv2dims.width + 1)};
  auto c            = zeros<float>(cdims);
  conv_forward_valid(b, bdims, conv2, conv2dims, c, cdims);

  // relu
  relu4(c, cdims);

  // sub-sampling: average pooling
  const shape ddims = {cdims.num, cdims.depth, cdims.height / pool_size, cdims.width / pool_size};
  auto d            = zeros<float>(ddims);
  average_pool(c, cdims, pool_size, d, ddims);

  // reshape
  const shape ddims2 = {ddims.num, ddims.depth * ddims.height * ddims.width};

  // fully connected layer 1: matrix multiplication
  const shape edims = {ddims.num, fc1dims.width};
  auto e            = zeros<float>(edims);
  fully_forward(d, ddims2, fc1, fc1dims, e, edims);

  // relu
  relu2(e, edims);

  // fully connected layer 2: matrix multiplication
  const shape fdims = {edims.height, fc2dims.width};
  auto f            = zeros<float>(fdims);
  fully_forward(e, edims, fc2, fc2dims, f, fdims);

  argmax(f, fdims, out);

  delete[] a;
  delete[] b;
  delete[] c;
  delete[] d;
  delete[] e;
    return f; //delete[] f;
}


void backward_operation_gpu(float *x, float *conv1, const float *y1, float *dedy, float *dedw, float *dedx) {
    // pre-processing: 1 convolution layer forward propagation
    const shape ydims = {xdims.num, conv1dims.num, (xdims.height - conv1dims.height + 1),
                        (xdims.width - conv1dims.width + 1)};
    const int ydims_g[] = {xdims_g[0], (xdims_g[1] - conv1dims_g[0] + 1),
                        (xdims_g[2] - conv1dims_g[1] + 1), conv1dims_g[3]};
    
    auto y            = zeros<float>(ydims);
    conv_forward_valid(x, xdims, conv1, conv1dims, y, ydims);
    // relu layer
    relu4(y, ydims);
    float *devX, *devX_unroll, *tunroll;
    check_success(cudaMalloc(&devX, sizeof(float)*1000*28*28));
    check_success(cudaMalloc(&devX_unroll, sizeof(float)*1000*25*24*24));
    check_success(cudaMalloc(&tunroll, sizeof(float)*1000*25*24*24));
    check_success(cudaMemcpy(devX, x, sizeof(float)*1000*28*28,cudaMemcpyHostToDevice));
    
    //copy W to constant
    check_success(cudaMemcpyToSymbol(dev_cons_Wunroll,conv1, sizeof(float)*25*32));
    
    //launching unroll x. This kernel came from Feidong top group in 408 project.
    dim3 grid_unr(1000,1,1);
    dim3 block_unr(512,1,1);
    unroll<<<grid_unr,block_unr>>>(devX, devX_unroll, tunroll);
    

    
    int ysize = ydims.flattened_length();
    std::cout << ysize;
    float *devY1, *devY, *dev_DEDY;
    
    check_success(cudaMalloc(&devY1, sizeof(float)*ysize));
    check_success(cudaMalloc(&devY,  sizeof(float)*ysize));
    check_success(cudaMalloc(&dev_DEDY, sizeof(float)*ysize));

    dim3 grid( ceil(ysize/1024.0f) ,1,1);
    dim3 block(1024,1,1);
    
    check_success(cudaMemcpy(devY1, &y1[0], sizeof(float)*ysize, cudaMemcpyHostToDevice));
    check_success(cudaMemcpy(devY, &y[0], sizeof(float)*ysize, cudaMemcpyHostToDevice));
    
    //conv_backward_ygrad(y1, y, ydims, dedy);
    conv_backward_ygrad_gpu<<<grid,block>>>(devY1, devY, ysize, dev_DEDY);

    //relu4(dedy, ydims);
    relu4_gpu<<<grid,block>>>(dev_DEDY, ysize);
    check_success(cudaMemcpy(dedy, &dev_DEDY[0], sizeof(float)*ysize, cudaMemcpyDeviceToHost));
    
    int dedw_size = conv1dims.flattened_length();
    int dedx_size = xdims.flattened_length();
    int xsize = xdims.flattened_length();
    float *dev_DEDW, *dev_DEDX, *devW;

    check_success(cudaMalloc(&dev_DEDW, sizeof(float)*dedw_size));
    check_success(cudaMalloc(&dev_DEDX, sizeof(float)*dedx_size));
    check_success(cudaMalloc(&devW, sizeof(float)*25*32));
    check_success(cudaMemcpy(devW, &conv1[0], sizeof(float)*25*32, cudaMemcpyHostToDevice));
    //check_success(cudaMemcpyToSymbol(dev_cons_W,conv1, sizeof(float)*25*32));
    
    //unrollX, and unroll dE/DY instead of unrolling W
    //conv_backward_wgrad(x, xdims, conv1, conv1dims, ydims, dedy, dedw);
    
    
    float* dev_namit = conv_backward_DEDW_n_DEDX_gpu(devX, tunroll, xdims_g, dev_DEDY, ydims_g, dev_DEDW, conv1dims_g, dev_DEDX, devW); //tunroll;
    float* dev_zoe = gpu_backward(x,conv1,y1,dedy,dedw,dedx);
    cudaDeviceSynchronize();
    float* zoe = (float*)malloc(4*dedw_size);
    float* namit = (float*)malloc(4*dedw_size);
    check_success(cudaMemcpy(zoe, dev_zoe, sizeof(float)*dedw_size, cudaMemcpyDeviceToHost));
    check_success(cudaMemcpy(namit, dev_namit, sizeof(float)*dedw_size, cudaMemcpyDeviceToHost));
    std::cout << ysize;
    //compare_solution2(zoe, dedw_size, namit, dedw_size);
    
    
    
    check_success(cudaDeviceSynchronize());
    check_success(cudaMemcpy(dedw, &dev_DEDW[0], sizeof(float)*dedw_size, cudaMemcpyDeviceToHost));
    
    //unroll dE/dY and unroll W. Or can just do basic convolution
    //conv_backward_xgrad(x, xdims, conv1, conv1dims, ydims, dedy, dedx);
    check_success(cudaMemcpy(dedx, &dev_DEDX[0], sizeof(float)*dedx_size, cudaMemcpyDeviceToHost));
    
    /// relu layer
    relu4(dedw, conv1dims);
    relu4(dedx, xdims);
    
    delete[] y;
    
    
}

// Backward operation for the CNN, a combination of conv layer + average pooling + relu
void backward_operation_cpu(float *x, float *conv1, const float *y1, float *dedy, float *dedw, float *dedx) {
  // pre-processing: 1 convolution layer forward propagation
  const shape ydims = {xdims.num, conv1dims.num, (xdims.height - conv1dims.height + 1),
                       (xdims.width - conv1dims.width + 1)};
  auto y            = zeros<float>(ydims);
  conv_forward_valid(x, xdims, conv1, conv1dims, y, ydims);

  // relu layer
  relu4(y, ydims);

  conv_backward_ygrad(y1, y, ydims, dedy);
  relu4(dedy, ydims);
  conv_backward_wgrad(x, xdims, conv1, conv1dims, ydims, dedy, dedw);
  conv_backward_xgrad(x, xdims, conv1, conv1dims, ydims, dedy, dedx);

  /// relu layer
  relu4(dedw, conv1dims);
  relu4(dedx, xdims);

  delete[] y;
}

// compare the results from CPU and GPU
static void compare_solution_int(int *cpu, const int cpu_size, int *gpu, const int gpu_size) {
  const float tolerance = 0.01f;
  if (cpu_size != gpu_size) {
    std::cout << "The dimensions do not match." << cpu_size <<" vs "<< gpu_size <<"\n";
    return;
  }
  // element-wise comparison: only prints out the first error and halts
  for (const auto i : range(0, cpu_size)) {
    if (std::abs(cpu[i] - gpu[i]) > 0) {
      std::cout << "Element " << i << " does not match.\n";
      std::cout << "cpu="<< cpu[i] << ",gpu=" << gpu[i] <<"\n";
      return;
    }
  }
  std::cout << "All the elements match!\n";
}

static void compare_solution(float *cpu, const int cpu_size, float *gpu, const int gpu_size) {
    const float tolerance = 0.01f; //300.0f;
    if (cpu_size != gpu_size) {
        std::cout << "The dimensions do not match." << cpu_size <<" vs "<< gpu_size <<"\n";
        return;
    }
    std::cout << "Comparing " << cpu_size << " elements.\n";
    // element-wise comparison: only prints out the first error and halts
    for (const auto i : range(0, cpu_size)) {
        if (std::abs(cpu[i] - gpu[i]) > tolerance) {
            std::cout << "Element " << i << " does not match.\n";
            std::cout << "cpu="<< cpu[i] << ",gpu=" << gpu[i] <<"\n";
            return;
        }
    }
    std::cout << "All the elements match!\n";
}

int main(int argc, char **argv) {

  // Generate data into x and y
  float *x = allocate<float>(xdims);
  float *y = allocate<float>(rdims);
  generate_data(x, xdims);

  // print_array(x, xdims);

  // Generate model
  float *conv1 = allocate<float>(conv1dims);
  float *conv2 = allocate<float>(conv2dims);
  float *fc1   = allocate<float>(fc1dims);
  float *fc2   = allocate<float>(fc2dims);
  generate_convfilters(conv1, conv1dims);
  generate_convfilters(conv2, conv2dims);
  generate_convfilters(fc1, fc1dims);
  generate_convfilters(fc2, fc2dims);

  // generate output feature map for verification
  const shape y1dims = {xdims.num, conv1dims.num, (xdims.height - conv1dims.height + 1),
                        (xdims.width - conv1dims.width + 1)};
  float *y1          = allocate<float>(y1dims);
  generate_data(y1, y1dims);
  int *out = zeros<int>(FLAGS_batch_size);

  float *dedy = zeros<float>(y1dims);
  float *dedw = zeros<float>(conv1dims);
  float *dedx = zeros<float>(xdims);
    
  int *out_gpu = zeros<int>(FLAGS_batch_size);
  float *dedy_gpu = zeros<float>(y1dims);
  float *dedw_gpu = zeros<float>(conv1dims);
  float *dedx_gpu = zeros<float>(xdims);

  // Launch kernel
  // ----------------------------------------
  printf("Launching kernel\n");
        const auto start1 = now();
  float * t2 = forward_operation_gpu(x, conv1, conv2, fc1, fc2, out_gpu);
  backward_operation_gpu(x, conv1, y1, dedy_gpu, dedw_gpu, dedx_gpu);
        const auto end1 = now();
        const auto elapsed1 = std::chrono::duration<double, std::milli>(end1 - start1).count();
        std::cout << "Done in GPU with in = " << elapsed1 << " milliseconds\n";
  // ----------------------------------------
  //GPU kernels finished
    
  // Sequential code
  printf("Performing CPU computation\n");

  // get start time
  const auto start = now();

  std::cout << "performing forward operation\n";
  float* t1 = forward_operation_cpu(x, conv1, conv2, fc1, fc2, out);

  std::cout << "performing backward operation\n";
  backward_operation_cpu(x, conv1, y1, dedy, dedw, dedx);

  // get end time
  const auto end = now();

  // get elapsed time in milliseconds
  const auto elapsed = std::chrono::duration<double, std::milli>(end - start).count();

  std::cout << "Done in CPU with " << FLAGS_batch_size << " queries in "
            << "elapsed = " << elapsed << " milliseconds\n";

  // Verify correctness
  // ----------------------------------------
        //printf("cpu=%d, gpu=%d\n", out[0], out_gpu[0]);
        //printf("cpu=%d, gpu=%d\n", out[1], out_gpu[1]);
        //printf("cpu=%d, gpu=%d\n", out[2], out_gpu[2]);
        printf("cpu=%f, gpu=%f\n", dedw[3], dedw_gpu[3]);
        printf("cpu=%f, gpu=%f\n", dedw[4], dedw_gpu[4]);
        printf("cpu=%f, gpu=%f\n", dedw[5], dedw_gpu[5]);
    
    compare_solution_int(out, 1000, out_gpu, 1000);
    compare_solution(dedy, y1dims.flattened_length(), dedy_gpu, 1000*32*24*24);
    compare_solution(dedw, conv1dims.flattened_length(), dedw_gpu, 32*5*5);
    compare_solution(dedx, xdims.flattened_length(), dedx_gpu, 1000*28*28);

  // Free memory
  // ----------------------------------------
  delete[] x;
  delete[] y;
  delete[] conv1;
  delete[] conv2;
  delete[] fc1;
  delete[] fc2;
  delete[] y1;
  delete[] out;
  delete[] dedy;
  delete[] dedw;
  delete[] dedx;

  return 0;
}
